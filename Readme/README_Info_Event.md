## Application infos_events
infos_events regroupe toutes les ressources pour notre liste de contacts.

### Models (Modèles)
Tous nos modèles sont situés dans :
```
infos_events/models.py
```

info_events contient deux modèles:

##### Event (Evènements)
Event est le modèle qui contient toutes les informations à propos d'un évènement, du prix au lieu.

##### Manager
Manager est lemodèle qui contient toutes les informations à propos du manager en tant que contact; pouvoir le contacter par email.

Une fois que vous avez effectué une modification sur un modèles, assurez vous qu'il est toujours conforme au formulaire admin : 
```teamspotapp/admin.py````

### Views (Vues)
Toutes nos vues sont situés dans :
```
infos_events/views.py
```

Nos vues ne sont pas enregistrés comme des vues REST. Nous pouvons en ajouter des personalisées dans les fichiers correspondant and les reliés dans le fichier urls.py.
Assurez vous de d'octroyer les permissions de superuser aux vues que vous ajoutez. A cause de l'importance de cex informations, seul un administrateur doit être apte à les octroyer.

