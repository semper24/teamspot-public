## api_rest de l'Application
Ceci est l'API REST pour le serveur TeamSpot.

Notre API est basé sur le standard [services web RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer).
Chaque endpoint est une ressource qui est accessible depuis une requête HTTP.

Notre code est structuré de manière à avoir des _Sérialiseurs_ et des _Ensembles de Vues_ pour chaque modèle existant.

### Sérialiseurs
Tous nos Sérialiseurs sont situé dans :
```
api_rest/serializers.py
```

Les Sérialiseurs sont utilisés pour convertir les données stockées dans la base de donnée dans un format qui correspond à ce que veut le client.
Une fois que le nouveau modèle est ajouté, un nouveau Sérialiseurs est aussi créer pour lui.

Un Sérialiseurs peut aussi être ajouté pour un besoin specifique mais ce choix doit être justifié.

### Ensemble de Vues (ViewSets)
Toutes nos ViewSets sont situées dans :
```
api_rest/viewsets.py
```

Les _ViewSets_ définissent des endpoints pour chaque modèle. Chaque _ViewSet_ fournira initialement une liste de requêtes comme POST/GET/PUT/DELETE/PATCH à éditer pour le modèle concerné.

Chaque _ViewSet_ est associé à un URL dans :
```
api_rest/urls.py
```

Une fois que le _ViewSet_ est associé à un URL, vous accédez à un évantail de requêtes basiques associées.
Toutefois, dans le _ViewSet_ vous pouvez ajouter des vues personalisées qui seront aussi accessibles depuis l'endpoint/URL.

Quand vous ajoutez une nouvelle vue, vous devez vous assurer de l'ajouter au _ViewSet_ correspondant.

### Authentication
Nous utilisons le framework REST de Django pour l'authentification système.

Cela signifie que toutes nos requêtes doivent être authentifiées.
Chaque requête doit donc contenir un token valide dans son header.
Le format est le suivant :
```
Authorization: Token <token>
```

Pour être sûr que votre _ViewSet_ va filtrer les requêtes, vous devez ajouter ceci à votre _ViewSet_ : 
```python
permission_classes = (IsAuthenticated,)
```

Vous pouvez visiter la documentation de notre API en ligne sur https://teamspot-server.fr/api_rest/docs/
