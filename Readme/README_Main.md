# TeamSpot

## Comment lancer le serveur de TeamSpot sur Linux
### Installations
##### Python
Tout d'abord, assurez-vous que vous avez installé Python.
```
python3 --version 
```
Si le résultat est : Python 3.7.7, je vous invite à passer à la partie Pip.
Pour ceux qui n'ont pas encore installé python, ou qui ont une mauvaise version de Python.
Veuillez installer python de cette façon :
```
Fedora:
dnf install python3
Ubuntu:
apt install python3
```
Maintenant, quand vous faites :
```
python3 --version 
```
La sortie devrait être : Python 3.7.7.
##### pip
Tout d'abord, assurez-vous que vous avez installé pip.
```
pip3 --version 
```
Si le résultat est : pip 19.0.3, je vous invite à passer à la partie suivante.
Pour ceux qui n'ont pas encore installé pip, ou qui ont une mauvaise version de pip.
Veuillez installer pip de cette façon :
```
Fedora:
dnf install python3-pip
Ubuntu:
apt install python3-pip
```
Maintenant, quand vous faites :
```
pip3 --version
```
La sortie devrait être : pip 19.0.3.

### Environnement Virtuel
Un environnement virtuel est un environnement Python tel que l'interpréteur, les bibliothèques et les scripts Python qui y sont installés sont isolés de ceux installés dans d'autres environnements virtuels.

#### Installation
Pour installer les paquets virtual env, lancez la commande suivante :


##### Ubuntu
```bash
sudo apt-get install python3-venv
```
##### Fedora
```bash
sudo dnf install python3-virtualenv
```
#### Creation
Avant de commencer à faire quoi que ce soit d'autre, vous devez créer votre propre environnement virtuel.
Allez dans la racine du référentiel et exécutez la ligne de commande suivante.
```
python3 -m venv venv
```
Maintenant, il devrait y avoir un nouveau répertoire nommé "venv" à la racine de votre repository.
#### Début
Un environnement par défaut a été installé sur le projet, pour le démarrer vous devez aller à la racine du projet, et faire :
```
source venv/bin/activate
```
Votre terminal pourrait voir quelques modifications, et vous pourriez être en mesure de voir quelque chose comme ```(venv) ->```.
> :warning : **Chaque fois que vous voulez travailler sur le projet ou installer quelque chose, vous devez démarrer l'ENV**.
### Dépendances
Vous pouvez installer toutes les dépendances sur votre environnement virtuel avec la ligne de commande suivante :
```
pip3 install -r requirements.txt
```
> **Maintenant toutes les dépendances devraient avoir été installées sur votre environnement virtuel.**
### Lancement
Tout d'abord, assurez-vous que toutes les migrations ont été effectuées avec :
```
python3 manage.py makemigrations
```
Vous devez ensuite propager toutes les modifications qui ont été apportées à vos modèles avec :
```
python3 manage.py migrate
```
Enfin, vous pouvez exécuter le serveur avec :
```
python3 manage.py runserver
```
Rendez-vous sur : http://localhost:8000/

### Production
Pour l'instant, vous n'avez utilisé le serveur que dans un environnement de test. Mais le serveur est lancé dans une machine virtuelle dans un environnement de production.

Vous pouvez accéder au serveur avec l'url donnée dans votre navigateur web préféré ou avec le client de votre choix :

#### [https://teamspot-server.fr](https://teamspot-server.fr)


### Tests
En ce qui concerne les tests des unités, vous devez les écrire dans leur fichier respectif situé dans chaque dossier correspondant :
```
teamspotapp/tests/
api_rest/tests/
infos_events/tests/
```
```tests_views.py``` sera réservé aux vues de test seulement.
```tests_models.py``` sera réservé aux tests de modèles seulement.
```tests_forms.py``` sera réservé aux tests de formulaires seulement.

Pour lancer les tests, vous devez soit vous exécuter à la racine :
```
python3 manage.py test <chemin_vers_le_dossier_de_test>
ou
coverage run --source='./teamspotapp' --omit="teamspotapp/tests/*" manage.py test teamspotapp/tests -v 2
```
Pour obtenir une analyse de votre test en fonction de la couverture que vous pouvez faire :
```
coverage report
```

### Mise en place
Pour commencer à programmer localement, vous devez mettre en place quelques éléments.

Tout d'abord, vous devez être en mesure de naviguer localement à travers votre API. Pour ce faire, nous vous conseillons de mettre à jour notre gestion de HTTPS dans :
```
TeamSpot/settings.py
```

Vous pouvez remplacer :

```
# # SSL PROXY
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SAMESITE = 'Strict'
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_SSL_REDIRECT = True
X_FRAME_OPTIONS = 'DENY'
SECURE_HSTS_SECONDS = 15768000
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
```

par

```
# # SSL PROXY
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SAMESITE = 'Strict'
SECURE_BROWSER_XSS_FILTER = False
SECURE_CONTENT_TYPE_NOSNIFF = False
SECURE_SSL_REDIRECT = False
X_FRAME_OPTIONS = 'DENY'
SECURE_HSTS_SECONDS = 15768000
SECURE_HSTS_INCLUDE_SUBDOMAINS = False
SECURE_HSTS_PRELOAD = False
```

Deuxièmement, vous devrez peut-être créer un superutilisateur qui vous permettra d'accéder à l'administration.
```
python manage.py createsuperuser
```
Une fois que votre superutilisateur est créé, assurez-vous de vous souvenir de vos informations d'identification.
Vous pouvez ensuite accéder à l'administration à l'adresse http://localhost:8000/admin.teamspot/.

Ici, vous aurez un aperçu de toutes les bases de données et de ce qu'elles contiennent. Vous pourrez également effectuer des actions telles que l'ajout, la modification et la suppression. Il est souvent intéressant de configurer manuellement vos bases de données pour tester des cas d'utilisation spécifiques et gagner du temps.

Maintenant que tout est configuré, vous pouvez lancer le serveur comme vous l'avez appris auparavant et visiter notre API à l'adresse suivante
http://localhost:8000/api_rest/docs/

### Prendre par au projet
N'hésitez pas à visiter les README.md correspondants dans les dossiers :
```
api_rest/
teamspotapp/
infos_events/
```
Vous pourrez donc contribuer au projet.
