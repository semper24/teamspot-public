## Application teamspotapp
```teamspotapp``` enregistre tous les modèles de TeamSpot.

Un modèle correspond à une table dans la base de données. Grâce à **SQLite3**, une base de données relationnelle, nous sommes en mesure de manipuler les tables de notre base de données comme des objets.

TeamSpot a établi un total de 14 modèles pour ses besoins. Il ne s'agit pas d'un nombre fixe, il peut changer à l'avenir.

#### CustomUser
Il hérite de AbstractUser (l'utilisateur par défaut de Django). Il représente chaque utilisateur de notre application.
Nous avons pensé TeamSpot de telle sorte que chaque personne ne doit avoir qu'un **un** compte.

#### File
Il stocke toutes sortes de fichiers, plus particulièrement les fichiers utilisés comme *fichier attaché* pour les e-mails et les messages dans l'application.
Nous l'utilisons également pour stocker nos enregistrements audio pour la section rapport.

#### TaskInfo
Il stocke toutes les tâches assignées à un ou plusieurs utilisateurs. TaskInfo permet d'obtenir plus d'informations sur la tâche, comme sa localisation, son importance et son échéance.

#### TaskLogInfo
Il stocke les mêmes informations que *TaskInfo* ; cependant, il correspond aux tâches terminées qui seront utilisées lors du post-mortem (après l'événement).

#### TeamInfo
Il enregistre toutes les *infos utilisateur* dans les équipes avec nom et description. Ainsi, nous pouvons personnaliser nos équipes et les rendre très spécifiques en fonction de l'événement.

#### RoleInfo
Il enregistre certains attributs qui donnent des droits pour un certain rôle. Par exemple, un manager se verra accorder beaucoup plus de droits qu'un bénévole.

#### ReportInfo
Il correspond à tous les rapports d'un utilisateur de notre application, ces rapports attendent d'être analysés pour devenir des *TaskInfo*. Ils peuvent être définis par des écritures ou des enregistrements.

#### MessageInfo
Il stocke tous les messages envoyés d'un *UserInfo* à un autre.

#### PlannedActivity
Il enregistre une activité unique qui a déjà été planifiée avant le début de l'événement.

#### Agenda
Il stocke quelques *PlannedActivity* et les associe à un *UserInfo* afin de créer un Agenda.

#### Notification
Il stocke une seule notification avec quelques détails tels que son nom, sa description et son type.

#### Notifications
Il enregistre une liste de *Notification* avec un *UserInfo* afin que nous puissions garder une trace de toutes les notifications attribuées à un utilisateur.

#### UserInfo
Il représente un utilisateur dans un événement, à ne pas confondre avec le *CustomUser*. Chaque *CustomUser* peut être associé à plusieurs *UserInfo* puisqu'ils ne seront associés qu'à un seul événement à la fois. Les *UserInfo* vont stocker un grand nombre d'informations concernant un utilisateur, plus particulièrement ses *RoleInfo* et *TeamInfo*. Ainsi que, sous forme de liste, *MessageInfo* et *TaskInfo*.

#### Event
C'est le noyau de chaque événement TeamSpot. Il stocke tous les éléments nécessaires à un événement et établit des liens entre eux.

### To add your contribution
Pour ajouter, modifier ou supprimer un modèle, vous devez vous rendre sur :
```teamspotapp/models.py```

Une fois que vous avez apporté une modification à un modèle, assurez-vous qu'il est toujours conforme au formulaire d'administration à :
```teamspotapp/admin.py````

Si vous avez créé un nouveau modèle, vous devez ajouter un ``list_display`` correspondant aux champs de votre modèle et l'enregistrer dans l'admin. Vous pouvez suivre ce qui a été fait pour tous les autres modèles. Cela devrait être à peu près la même chose.


